extends KinematicBody

var velocidade = Vector3(0,0,0)
const SPEED = 6
const ROTSPEED = 7 
	
func _physics_process(delta):
	
	if Input.is_action_pressed("ui_right") and Input.is_action_pressed("ui_left"):
		velocidade.x = 0
	elif Input.is_action_pressed("ui_right"):
		velocidade.x = SPEED
		$MeshInstance.rotate_z(deg2rad(-ROTSPEED))
	elif Input.is_action_pressed("ui_left"):
		velocidade.x = -SPEED
		$MeshInstance.rotate_z(deg2rad(ROTSPEED))
	else:
		velocidade.x = lerp(velocidade.x,0,0.1)
		
	if Input.is_action_pressed("ui_up") and Input.is_action_pressed("ui_down"):
		velocidade.z = 0
	elif Input.is_action_pressed("ui_up"):
		velocidade.z = -SPEED
		$MeshInstance.rotate_x(deg2rad(-ROTSPEED))
	elif Input.is_action_pressed("ui_down"):
		velocidade.z = SPEED
		$MeshInstance.rotate_x(deg2rad(ROTSPEED))
	else:
		velocidade.z = lerp(velocidade.z,0,0.1)
	move_and_slide(velocidade)
		
		
func _ready():
	pass
